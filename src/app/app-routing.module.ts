import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RoleListComponent } from './components/role-list/role-list.component';
import { RoleResolverService } from './services/role-resolver.service';
import { RoleComponent } from './components/role/role.component';
import { AddRoleComponent } from './components/role/add-role/add-role.component';
import { EditRoleComponent } from './components/role/edit-role/edit-role.component';
import { CloneRoleComponent } from './components/role/clone-role/clone-role.component';
import { ViewRoleComponent } from './components/role/view-role/view-role.component';

const routes: Routes = [
  {
    path: '',
    component: RoleListComponent,
  },
  {
    path: 'new-role',
    component: AddRoleComponent,
  },
  {
    path: 'role/:id',
    component: RoleComponent,
    resolve: {
      role: RoleResolverService,
    },
    children: [
      {
        path: '',
        component: ViewRoleComponent,
      },
      {
        path: 'edit',
        component: EditRoleComponent
      },
      {
        path: 'clone',
        component: CloneRoleComponent,
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
