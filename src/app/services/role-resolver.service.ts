import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Role } from '../interfaces/role';
import { RoleService } from './role.service';

@Injectable({
  providedIn: 'root'
})
export class RoleResolverService implements Resolve<Role> {

  constructor(
    private roleService: RoleService,
  ) { }

  resolve(route: ActivatedRouteSnapshot) {
    if (route.params.id !== undefined) {
      return this.roleService.getRole(route.params.id);
    }
    return null;
  }
}
