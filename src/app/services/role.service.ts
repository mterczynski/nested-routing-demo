import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Role } from '../interfaces/role';

@Injectable({
  providedIn: 'root'
})
export class RoleService {

  getRole(id: string) {
    console.log('calling server to get single role');
    return new Observable<Role>((observer) => {
      setTimeout(() => {
        observer.next({
          id,
          name: 'Admin',
          description: 'Super admin of all applications',
          permissions: [
            {
              id: 'permission-1',
              categoryId: 'category-1',
              name: 'Adding post',
              description: 'Allows user to add new post',
            }
          ],
        });
        observer.complete();
      }, 1000);
    });
  }

  getRoles() {
    console.log('calling server to get all roles');
    return new Observable<Role[]>((observer) => {
      setTimeout(() => {
        observer.next([
          // role 1:
          {
            id: '1',
            name: 'Admin',
            description: 'Super admin of all applications',
            permissions: [
              {
                id: 'permission-1',
                categoryId: 'category-1',
                name: 'Adding post',
                description: 'Allows user to add new post',
              }
            ],
          },
          // role 2:
          {
            id: '2',
            name: 'Doctor',
            description: 'Heals and writes prescriptions',
            permissions: [
              {
                id: 'permission-1',
                categoryId: 'category-1',
                name: 'Adding post',
                description: 'Allows user to add new post',
              }
            ],
          }
        ]);
        observer.complete();
      }, 1000);
    });
  }
}
