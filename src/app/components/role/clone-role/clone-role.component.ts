import { Component, OnInit } from '@angular/core';
import { Role } from 'src/app/interfaces/role';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-clone-role',
  templateUrl: './clone-role.component.html',
  styleUrls: ['./clone-role.component.scss']
})
export class CloneRoleComponent implements OnInit {
  private role: Role;

  constructor(
    private route: ActivatedRoute,
    public router: Router
  ) { }

  ngOnInit() {
    this.role = this.route.snapshot.data.role;
  }
}
