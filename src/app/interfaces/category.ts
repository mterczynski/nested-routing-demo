export interface Category {
  id: string;
  name: string;
  appId: string;
}
