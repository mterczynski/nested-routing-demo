export interface Permission {
  id: string;
  categoryId: string;
  name: string;
  description: string;
}
