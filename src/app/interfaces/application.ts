import { Permission } from './permission';

export interface Application {
  id: string;
  name: string;
  permissions: Permission[];
}
