import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RoleComponent } from './components/role/role.component';
import { RoleListComponent } from './components/role-list/role-list.component';
import { AddRoleComponent } from './components/role/add-role/add-role.component';
import { EditRoleComponent } from './components/role/edit-role/edit-role.component';
import { CloneRoleComponent } from './components/role/clone-role/clone-role.component';
import { ViewRoleComponent } from './components/role/view-role/view-role.component';

@NgModule({
  declarations: [
    AppComponent,
    RoleComponent,
    RoleListComponent,
    AddRoleComponent,
    ViewRoleComponent,
    EditRoleComponent,
    CloneRoleComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
